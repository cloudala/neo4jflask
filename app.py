# Restart flask server on save: flask run --reload
from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
# Allows us to access environmental variables
import os

# Loading environmental variables from the .env file
load_dotenv()
uri = os.getenv("URI")
username = os.getenv("USER")
password = os.getenv("PASSWORD")

# Creating a neo4j driver
driver = GraphDatabase.driver(uri, auth=(username, password), database="neo4j")

app = Flask(__name__)

# Getting all employees
def get_employees(tx, request_arguments):
    sortBy = request_arguments.get("sortBy")
    sortOrder = request_arguments.get("sortOrder")
    sortDirection = sortOrder if sortOrder else "ASC"
    request_keys = request_arguments.keys()
    filter_fields = ["name", "surname", "position"]
    filters = []

    # Filtering logic
    for field in filter_fields:
        if field in request_keys:
            filters.append(f"e.{field}='{request_arguments[field]}'")

    # Sorting logic
    sort_clause = f"ORDER BY e.{sortBy} {sortDirection}" if sortBy else ""

    # Constructing the query
    query = f"MATCH (e:Employee)"
    if filters:
        query += f" WHERE {' AND '.join(filters)}"
    query += f" RETURN e {sort_clause}"

    # Running the query
    results = tx.run(query).data()
    employees = [result['e'] for result in results]
    return employees

@app.route("/employees", methods = ["GET"])
def get_employees_route():
    request_arguments = request.args
    with driver.session() as session:
        employees = session.read_transaction(get_employees, request_arguments)
    response = {'employees': employees}
    return jsonify(response), 200

# Adding a new employee
def employee_exists(tx, name, surname):
    # Checking if employee with name and surname already exists in the database
    query_check = "MATCH (e:Employee {name: $name, surname: $surname}) RETURN e"
    return tx.run(query_check, name=name, surname=surname).single() is not None

def create_employee(tx, name, surname, position, department):
    relation = "MANAGES" if position == "Manager" else "WORKS_IN"
    query = (
        "MERGE (d:Department {name: $department}) "
        "MERGE (e:Employee {name: $name, surname: $surname, position: $position}) "
        f"MERGE (e)-[:{relation}]->(d)"
    )
    tx.run(query, name=name, surname=surname, position=position, department=department)

@app.route("/employees", methods = ["POST"])
def post_employees_route():
    data = request.get_json()
    name = data.get("name")
    surname = data.get("surname")
    position = data.get("position")
    department = data.get("department")
    if name and surname and position and department:
        with driver.session() as session:
            if employee_exists(session, name, surname):
                response = {"message": "Employee already exists!"}
                return jsonify(response), 409
            else:
                session.write_transaction(create_employee, name, surname, position, department)
                response = {'success': "Employee added successfully!"}
                return jsonify(response), 201
    else:
        response = {"message": "Invalid input!"}
        return jsonify(response), 405

# Updating employee data
def employee_exists_by_id(tx, id):
    # Checking if employee with id already exists in the database
    query_check = "MATCH (e:Employee) WHERE ID(e) = $id RETURN e"
    return tx.run(query_check, id=id).single() is not None

def update_employees(tx, id, data):
    employee_fields = ["name", "surname", "position"]
    fields_to_update = []

    # Appending fields to update to the list
    for employee_field in employee_fields:
        if employee_field in data:
            fields_to_update.append(f"e.{employee_field}='{data[employee_field]}'")
    
    # Check current position and department
    checkPositionAndDepartment = """MATCH (e:Employee)-[oldRel]->
                                (oldDepartment:Department)
                                WHERE ID(e) = $id RETURN TYPE(oldRel), oldDepartment"""
    
    currentPosition = tx.run(checkPositionAndDepartment, id=id).data()[0]["TYPE(oldRel)"]
    currentDepartment = tx.run(checkPositionAndDepartment, id=id).data()[0]["oldDepartment"]

    # Constructing the query
    query = """MATCH (e:Employee)-[oldRel]->(oldDepartment:Department)
            WHERE ID(e) = $id"""
    
    if data["position"]:
        newPosition = "MANAGES" if data["position"] == "Manager" else "WORKS_IN"
    
    # Updating position and department if needed
    if data["position"] and data["department"]:
        query+=f"""
                DELETE oldRel
                WITH e
                MERGE (newDepartment:Department {{name: "{data["department"]}"}})
                MERGE (e)-[:{newPosition}]->(newDepartment)
                """
    elif data["position"]:
        query+=f"""
                DELETE oldRel
                WITH e
                MERGE (currentDepartment:Department {{name: "{currentDepartment}"}})
                MERGE (e)-[:{newPosition}]->(currentDepartment)
                """
    elif data["department"]:
        query+=f"""
                DELETE oldRel
                WITH e
                MERGE (newDepartment:Department {{name: "{data["department"]}"}})
                MERGE (e)-[:{currentPosition}]->(newDepartment)
                """
        
    if fields_to_update:
        query += f" SET {', '.join(fields_to_update)}"

    query += f" RETURN e"

    tx.run(query, id=id)

@app.route("/employees/<int:id>", methods = ["PUT"])
def put_employees_route(id):
    data = request.get_json()
    print(id)
    print(data)
    if id and data:
        with driver.session() as session:
            if not employee_exists_by_id(session, id):
                response = {"message": "Employee doesn't exist!"}
                return jsonify(response), 404
            else:
                session.write_transaction(update_employees, id, data)
                response = {'success': "Employee updated successfully"}
                return jsonify(response), 200
    else:
        response = {"message": "Invalid request input!"}
        return jsonify(response), 405

# Deleting an employee
def delete_employee(tx, id):
    # Checking if the employee is a Manager
    query = """
            MATCH (e:Employee)-[rel]->(d:Department)
            WHERE ID(e) = $id
            RETURN d, TYPE(rel) AS relationshipType
            """
    employeeOrManager = tx.run(query, id=id).data()[0]["relationshipType"]
    department = tx.run(query, id=id).data()[0]["d"]["name"]

    # Employee is not a Manager
    if employeeOrManager == "WORKS_IN":
        query = """
                MATCH (e:Employee)-[rel]->(d:Department)
                WHERE ID(e) = $id
                DETACH DELETE e
                """
        tx.run(query, id=id)
    else:
        findManagerQuery = """
                            MATCH (e:Employee)-[:MANAGES]->(d:Department)
                            WHERE ID(e) = $id
                            MATCH (employees:Employee)-[:WORKS_IN]->(d)
                            RETURN ID(employees) AS newManagerId LIMIT 1
                           """
        foundManager = tx.run(findManagerQuery, id=id).data()
        if foundManager:
             # Appoint new manager
            newManagerId = foundManager[0]["newManagerId"]
            deleteManagerandAppointNewManagerQuery = f"""
                MATCH (manager:Employee)-[rel]->(d:Department {{name: "{department}"}})
                WHERE ID(manager) = $id
                DELETE rel, manager
                WITH manager
                MATCH (newManager:Employee)-[works:WORKS_IN]->(d)
                WHERE ID(newManager) = {newManagerId}
                DELETE works
                CREATE (newManager)-[:MANAGES]->(d)
            """
            tx.run(deleteManagerandAppointNewManagerQuery, id=id)
        else:
            # Delete department
            deleteManagerandDepartmentQuery = f"""
                            MATCH (e:Employee)-[relation]->(d:Department {{name: "{department}"}})
                            WHERE ID(e) = $id
                            DELETE relation, e, d
                           """
            tx.run(deleteManagerandDepartmentQuery, id=id)

@app.route("/employees/<int:id>", methods=["DELETE"])
def delete_employee_route(id):
    with driver.session() as session:
        if not employee_exists_by_id(session, id):
            response = {"message": "Employee doesn't exist!"}
            return jsonify(response), 404
        else:
            session.write_transaction(delete_employee, id)
            response = {'message': "Employee deleted successfully!"}
            return jsonify(response), 200

# Getting the subordinates of a manager
def get_subordinates(tx, id):
    # Constructing the query
    query = """
            MATCH (e:Employee)-[:MANAGES]->(d:Department) 
            WHERE ID(e) = $id
            MATCH (employee:Employee)-[:WORKS_IN]->(d)
            RETURN employee
            """

    # Running the query
    results = tx.run(query, id=id).data()
    employees = [result['employee'] for result in results]
    return employees

@app.route("/employees/<int:id>/subordinates", methods = ["GET"])
def get_subordinates_route(id):
    with driver.session() as session:
        subordinates = session.read_transaction(get_subordinates, id)
    response = {'subordinates': subordinates}
    return jsonify(response), 200

# Getting information about an employee's department
def get_employee_department(tx, id):
    # Constructing the query
    query = """
            MATCH (employee:Employee)-[:WORKS_IN]->(d:Department)
            WHERE ID(employee) = $id
            MATCH (e:Employee)-[:WORKS_IN]->(d) 
            MATCH (m:Employee)-[:MANAGES]->(d)
            RETURN d.name AS name, COUNT(e) AS `number of employees`, m AS manager
            """

    # Running the query
    department = tx.run(query, id=id).data()[0]
    return department

@app.route("/employees/<int:id>/departments", methods = ["GET"])
def get_employee_department_route(id):
    with driver.session() as session:
        department = session.read_transaction(get_employee_department, id)
    response = {'department': department}
    return jsonify(response), 200

# Getting all departments
def get_departments(tx, request_arguments):
    sortBy = request_arguments.get("sortBy")
    sortOrder = request_arguments.get("sortOrder")
    sortDirection = sortOrder if sortOrder else "ASC"
    request_keys = request_arguments.keys()
    filter_fields = ["name", "number of employees", "manager.name", "manager.surname", "manager.position"]
    filters = []

    # Filtering logic
    for field in filter_fields:
        if field in request_keys:
            filters.append(f"{field}='{request_arguments[field]}'")

    # # Sorting logic
    sort_clause = f"ORDER BY {sortBy} {sortDirection}" if sortBy else ""

    # Constructing the query
    query = """
            MATCH (d:Department)
            MATCH (e:Employee)-[:WORKS_IN]->(d) 
            MATCH (m:Employee)-[:MANAGES]->(d)
            WITH d.name AS name, COUNT(e) AS `number of employees`, m AS manager
            """
    if filters:
        query += f" WHERE {' AND '.join(filters)}"
    query += f" RETURN name, `number of employees`, manager {sort_clause}"

    # Running the query
    results = tx.run(query).data()
    return results

@app.route("/departments", methods = ["GET"])
def get_departments_route():
    request_arguments = request.args
    with driver.session() as session:
        departments = session.read_transaction(get_departments, request_arguments)
    response = {'departments': departments}
    return jsonify(response), 200

# Getting department employees
def get_department_employees(tx, id):
    # Constructing the query
    query = """
            MATCH (e:Employee)-[:WORKS_IN]->(d:Department) 
            WHERE ID(d) = $id
            RETURN e
            """
    # Running the query
    results = tx.run(query, id=id).data()
    employees = [result['e'] for result in results]
    return employees

@app.route("/departments/<int:id>/employees", methods = ["GET"])
def get_department_employees_route(id):
    with driver.session() as session:
        employees = session.read_transaction(get_department_employees, id)
    response = {'Department employees': employees}
    return jsonify(response), 200

# Running our app (provided its the main programme and isn't imported as a module)
if __name__ == "__main__":
    app.run()