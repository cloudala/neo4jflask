// Creating Departments
CREATE (it:Department {name: 'IT'})
CREATE (law:Department {name: 'Law'})
CREATE (finance:Department {name: 'Finance'})
CREATE (marketing:Department {name: 'Marketing'})
CREATE (hr:Department {name: 'HR'})

// Creating Employees (assign to existing departments)
// IT Department
// Manager
CREATE (itmanager:Employee {name: 'Eva', surname: 'Davis', position: 'Manager'})-[:MANAGES]->(it)
// Other employees
CREATE (:Employee {name: 'Alice', surname: 'Johnson', position: 'Software Engineer'})-[:WORKS_IN]->(it)
CREATE (:Employee {name: 'Bob', surname: 'Williams', position: 'Data Scientist'})-[:WORKS_IN]->(it)
CREATE (:Employee {name: 'Sophia', surname: 'Johnson', position: 'Software Developer'})-[:WORKS_IN]->(it)
CREATE (:Employee {name: 'Ethan', surname: 'Davis', position: 'Systems Engineer'})-[:WORKS_IN]->(it)

// Law Department
// Manager
CREATE (lawmanager:Employee {name: 'Michael', surname: 'Brown', position: 'Manager'})-[:MANAGES]->(law)
// Other employees
CREATE (:Employee {name: 'Liam', surname: 'Johnson', position: 'Legal Counsel'})-[:WORKS_IN]->(law)
CREATE (:Employee {name: 'Sophie', surname: 'Miller', position: 'Paralegal'})-[:WORKS_IN]->(law)
CREATE (:Employee {name: 'David', surname: 'Wilson', position: 'Legal Analyst'})-[:WORKS_IN]->(law)
CREATE (:Employee {name: 'Mia', surname: 'Miller', position: 'Legal Research Assistant'})-[:WORKS_IN]->(law)

// Finance Department
// Manager
CREATE (financemanager:Employee {name: 'Emma', surname: 'Davis', position: 'Manager'})-[:MANAGES]->(finance)
// Other Employees
CREATE (:Employee {name: 'Olivia', surname: 'Smith', position: 'Financial Analyst'})-[:WORKS_IN]->(finance)
CREATE (:Employee {name: 'Charlie', surname: 'Brown', position: 'Accountant'})-[:WORKS_IN]->(finance)
CREATE (:Employee {name: 'Charlotte', surname: 'Williams', position: 'Financial Planner'})-[:WORKS_IN]->(finance)
CREATE (:Employee {name: 'Noah', surname: 'Davis', position: 'Senior Accountant'})-[:WORKS_IN]->(finance)

// Marketing Department
// Manager
CREATE (marketingmanager:Employee {name: 'Owen', surname: 'Wilson', position: 'Manager'})-[:MANAGES]->(marketing)
// Other employees
CREATE (:Employee {name: 'Lily', surname: 'Johnson', position: 'Marketing Specialist'})-[:WORKS_IN]->(marketing)
CREATE (:Employee {name: 'Zoe', surname: 'Davis', position: 'Content Writer'})-[:WORKS_IN]->(marketing)
CREATE (:Employee {name: 'Liam', surname: 'Davis', position: 'Social Media Consultant'})-[:WORKS_IN]->(marketing)
CREATE (:Employee {name: 'Avery', surname: 'Wilson', position: 'Marketing Coordinator'})-[:WORKS_IN]->(marketing)

// Human Resources Department
// Manager
CREATE (hrmanager:Employee {name: 'Jackson', surname: 'Brown', position: 'Manager'})-[:MANAGES]->(hr)
// Other employees
CREATE (:Employee {name: 'Chloe', surname: 'Williams', position: 'HR Specialist'})-[:WORKS_IN]->(hr)
CREATE (:Employee {name: 'Lucy', surname: 'Smith', position: 'Recruitment Coordinator'})-[:WORKS_IN]->(hr)
CREATE (:Employee {name: 'Elijah', surname: 'Miller', position: 'Employee Relations Consultant'})-[:WORKS_IN]->(hr)
CREATE (:Employee {name: 'Grace', surname: 'Davis', position: 'HR Assistant'})-[:WORKS_IN]->(hr)